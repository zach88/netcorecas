﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;

namespace NetCoreTest
{
    public interface IDbContext { }
    public partial class AnacafeContext : DbContext, IDbContext
    {
        public AnacafeContext()
        {
        }

        public AnacafeContext(DbContextOptions<AnacafeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ClasesCafe> ClasesCafe { get; set; }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<ClasesCafe>(entity =>
            {
                entity.HasKey(e => e.CodClaseCafe)
                    .HasName("PK_ClasesCafe_1__21");

                entity.Property(e => e.CodClaseCafe).HasColumnName("Cod_Clase_Cafe");

                entity.Property(e => e.ConversionOro).HasColumnName("Conversion_Oro");

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PartidaArancelaria)
                    .HasColumnName("Partida_Arancelaria")
                    .HasMaxLength(12)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}