﻿using System;
using System.Collections.Generic;

namespace NetCoreTest
{
    public partial class ClasesCafe
    {
        public byte CodClaseCafe { get; set; }
        public string Descripcion { get; set; }
        public float ConversionOro { get; set; }
        public string PartidaArancelaria { get; set; }
    }
}